// Package sliceutil contains utility functions for working with slices.
package sliceutil

import (
	"fmt"
)

// ShowSliceOfInt simply show a slice of integers  with capacity and values
func ShowSliceOfInt(s string, slice []int) {
	fmt.Printf("%s len=%d cap=%d %v com #%v em %p\n", s, len(slice), cap(slice), slice, slice, slice)
}

// ShowSliceOfStrings simply show a slice of strings  with capacity and values
func ShowSliceOfStrings(s string, slice []string) {
	fmt.Printf("%s len=%d cap=%d %v com #%v em %p\n", s, len(slice), cap(slice), slice, slice, slice)
}
